package DRONE;

import java.util.ArrayList;

public class Queue {
    public ArrayList<Delivery.Message> buffer = new ArrayList<>();

    public synchronized void put(Delivery.Message message){
        buffer.add(message);
        this.notify();
    }

    public synchronized Delivery.Message take(){
        Delivery.Message message = null;

        if (buffer.size() == 0){
            try {
                this.wait(15000);   // maximum wait = 15 seconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (buffer.size() > 0){
            message = buffer.get(0);
            buffer.remove(0);
        }
        return message;
    }

    public synchronized void putWithPriority(Delivery.Message update) {
        buffer.add(0, update);
        this.notify();
    }
}
