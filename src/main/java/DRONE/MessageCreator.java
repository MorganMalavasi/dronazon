package DRONE;

import BEANS.MapIdPort;
import BEANS.Order;
import SIMULATORI.Measurement;

import java.util.List;
import java.util.Random;

public class MessageCreator {
    public static Delivery.Message.Builder createMessageTopology(int to, long timestamp, List<MapIdPort> mapIdPorts, boolean close) {
        // -> create a new message to set the topology of the ring
        Delivery.TopologyMessage.Builder topologyMessage = Delivery.TopologyMessage.newBuilder();
        topologyMessage.setId(to);
        topologyMessage.setTimestamp(timestamp);
        topologyMessage.setNeedClosing(close);

        for (MapIdPort element : mapIdPorts) {
            topologyMessage.addArray(Delivery.TupleIdPort.newBuilder()
                    .setId(element.getId())
                    .setPort(element.getPort()));
        }

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 0);
        msg.setTopologyMessage(topologyMessage);

        return msg;
    }

    public static Delivery.Message.Builder createMessageElectionWhoIsMaster(int id, int master) {
        // -> create a new message to find who is the master
        Delivery.FindMaster.Builder findMaster = Delivery.FindMaster.newBuilder();
        findMaster.setId(id);
        findMaster.setMaster(master);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 1);
        msg.setFindMaster(findMaster);

        return msg;
    }

    public static Delivery.Message.Builder createMessageElectionStartElection(int id, int battery) {
        // -> create a new message to init a new election
        Delivery.Election.Builder newElection = Delivery.Election.newBuilder();
        newElection.setId(id);
        newElection.setBattery(battery);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 2);
        msg.setElection(newElection);

        return msg;
    }

    public static Delivery.Message.Builder createMessageRoundOfSignatures(int id, List<Boolean> signatures) {
        // -> create a new message to collect the signatures
        Delivery.RoundOfSignatures.Builder petition = Delivery.RoundOfSignatures.newBuilder();
        petition.setId(id);
        for (Boolean elem : signatures) petition.addSignatures(elem);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 3);
        msg.setRoundOfSignatures(petition);

        return msg;
    }

    public static Delivery.Message.Builder createMessageElectionElected(int id) {
        // -> create a new message to say who is the new coordinator
        Delivery.Elected.Builder elected = Delivery.Elected.newBuilder();
        elected.setId(id);
        elected.setTimestampElection(System.currentTimeMillis());

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 4);
        msg.setElected(elected);

        return msg;
    }

    public static Delivery.Message.Builder createMessageBatteryPositionStatus(int id, int battery, int posX, int posY, int master, int totKm, int totDeliveries) {
        // -> create a new message to update the master about the info of this drone
        Delivery.BatteryPositionStatus.Builder infos = Delivery.BatteryPositionStatus.newBuilder();
        infos.setId(id);
        infos.setBattery(battery);
        infos.setPosX(posX);
        infos.setPosY(posY);
        infos.setTo(master);
        infos.setTotKm(totKm);
        infos.setTotDeliveries(totDeliveries);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 5);
        msg.setBatteryPositionStatus(infos);

        return msg;
    }

    public static Delivery.Message.Builder createMessageDeliveryPackage(int sender, int receiver, int idPackage, int pick_upX, int pick_upY, int deliveryX, int deliveryY) {
        // -> create message to deliver a new package
        Delivery.DeliveryPackage.Builder newDelivery = Delivery.DeliveryPackage.newBuilder();
        newDelivery.setSender(sender);
        newDelivery.setReceiver(receiver);
        newDelivery.setIdPackage(idPackage);
        newDelivery.setPickUpX(pick_upX);
        newDelivery.setPickUpY(pick_upY);
        newDelivery.setDeliveryX(deliveryX);
        newDelivery.setDeliveryY(deliveryY);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 6);
        msg.setDeliveryPackage(newDelivery);

        return msg;
    }

    public static Delivery.Message.Builder createMessageDeliveryCompleted(int id, int battery, int posX, int posY, int to, long timestamp, int km, List<Measurement> measurementList, boolean cont) {
        // -> create message for updating the master
        Delivery.DeliveryCompleted.Builder newDeliveryUpdate = Delivery.DeliveryCompleted.newBuilder();
        newDeliveryUpdate.setId(id);
        newDeliveryUpdate.setBattery(battery);
        newDeliveryUpdate.setPosX(posX);
        newDeliveryUpdate.setPosY(posY);
        newDeliveryUpdate.setTo(to);
        newDeliveryUpdate.setTimestamp(timestamp);
        newDeliveryUpdate.setKmTraveled(km);

        for (Measurement element : measurementList) {
            newDeliveryUpdate.addMeasurement(Delivery.Measurement.newBuilder()
                    .setId(element.getId())
                    .setValue(element.getValue())
                    .setType(element.getType())
                    .setTimestamp(element.getTimestamp()));
        }

        newDeliveryUpdate.setContinue(cont);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 7);
        msg.setDeliveryCompleted(newDeliveryUpdate);

        return msg;
    }

    public static Delivery.Message.Builder createRechargeMessageAsk(int id, long timestamp) {
        // -> create message for asking to recharge the battery
        Delivery.RechargeMessage.Builder rechargeMessage = Delivery.RechargeMessage.newBuilder();
        rechargeMessage.setId(id);
        rechargeMessage.setTimestamp(timestamp);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 8);
        msg.setRechargeMessage(rechargeMessage);

        return msg;
    }

    public static Delivery.Message.Builder createRechargeMessageAnswer(int id) {
        // -> create message to answer to drone who asked the resource
        Delivery.AnswerRechargeMessage.Builder answerMessage = Delivery.AnswerRechargeMessage.newBuilder();
        answerMessage.setId(id);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 9);
        msg.setAnswerRechargeMessage(answerMessage);

        return msg;
    }

    public static Delivery.Message.Builder createAreYouOkMessageQuestion(int id) {
        // -> create message to ask if a drone is ok
        Delivery.AreYouOk.Builder areYouOk = Delivery.AreYouOk.newBuilder();
        areYouOk.setId(id);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 10);
        msg.setAreYouOk(areYouOk);

        return msg;
    }

    public static Delivery.Message.Builder createImOkMessageAnswer(int id) {
        // -> create message to answer
        Delivery.ImOk.Builder imOk = Delivery.ImOk.newBuilder();
        imOk.setId(id);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 11);
        msg.setImOk(imOk);

        return msg;
    }

    public static Delivery.Message.Builder createResetMasterDroneInternalStructureMessage(int id) {
        // -> create message for resetting the master drone internal structure
        Delivery.ResetInternalStructure.Builder reset = Delivery.ResetInternalStructure.newBuilder();
        reset.setId(id);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 12);
        msg.setResetInternalStructure(reset);

        return msg;
    }

    public static Delivery.Message.Builder createExitMessageQuestion(int id, long timestamp) {
        // -> create message for asking to exit from the query
        Delivery.ExitMessage.Builder exitMessage = Delivery.ExitMessage.newBuilder();
        exitMessage.setId(id);
        exitMessage.setTimestamp(timestamp);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 13);
        msg.setExitMessage(exitMessage);

        return msg;
    }

    public static Delivery.Message.Builder createExitMessageAnswer(int id) {
        // -> create message to answer to drone who asked the exit
        Delivery.AnswerExitMessage.Builder answerExitMessage = Delivery.AnswerExitMessage.newBuilder();
        answerExitMessage.setId(id);

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfo(msg, 14);
        msg.setAnswerExitMessage(answerExitMessage);

        return msg;
    }

    public static Delivery.Message.Builder createMessageDeliveryOrdersPending(List<Order> list) {
        Delivery.OrderPending.Builder ordersPending = Delivery.OrderPending.newBuilder();

        for (Order order : list) {
            ordersPending.addArray(Delivery.DeliveryPackage.newBuilder()
                    .setIdPackage(order.getIdPackage())
                    .setPickUpX(order.getPick_upX())
                    .setPickUpY(order.getPick_upY())
                    .setDeliveryX(order.getDeliveryX())
                    .setDeliveryY(order.getDeliveryY()));
        }

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();
        createBasicInfoWithoutCounter(msg, 15);
        msg.setOrderPending(ordersPending);

        return msg;
    }

    private static void createBasicInfo(Delivery.Message.Builder m, int type) {
        Random rand = new Random();
        int randIdentifier = rand.nextInt(100000);

        int sizeRing = GrpcClient.sizeRing();       // => is used to determine the max counter of the drone

        m.setIdMessage(randIdentifier);
        m.setType(type);
        m.setCounter(sizeRing * 3 + 1);             // => size * 3 + 1 is a personal choice, if the drone is not found then the package will run a maximum of 3 laps + 1.
        m.setTimestampCreation(System.currentTimeMillis());

        printSubLevelMessage("Created Message type = " + m.getType() + " nr. -> " + randIdentifier);
    }

    private static void createBasicInfoWithoutCounter(Delivery.Message.Builder m, int type) {
        Random rand = new Random();
        int randIdentifier = rand.nextInt(100000);

        m.setIdMessage(randIdentifier);
        m.setType(type);
        m.setCounter(100000);
        m.setTimestampCreation(System.currentTimeMillis());

        printSubLevelMessage("Created Message type = " + m.getType() + " nr. -> " + randIdentifier + " without counter");
    }


    //////////////////////////////////////// RECREATE MESSAGE ////////////////////////////////////////
    public static Delivery.Message.Builder recreateMessage(Delivery.Message m) {
        /*
              message Message{
              int32 idMessage = 1;
              int32 type = 2;
              int32 counter = 3;
              int64 timestampCreation = 4;

              TopologyMessage topologyMessage = 5;
              FindMaster findMaster = 6;
              Election election = 7;
              RoundOfSignatures roundOfSignatures = 8;
              Elected elected = 9;
              BatteryPositionStatus batteryPositionStatus = 10;
              DeliveryPackage deliveryPackage = 11;
              DeliveryCompleted deliveryCompleted = 12;
              RechargeMessage rechargeMessage = 13;
              AnswerRechargeMessage answerRechargeMessage = 14;
              AreYouOk areYouOk = 15;
              ImOk imOk = 16;
              ResetInternalStructure resetInternalStructure = 17;
              ExitMessage exitMessage = 18;
              AnswerExitMessage answerExitMessage = 19;
              OrderPending orderPending = 20;
         */

        int counterRemaining = m.getCounter() - 1;

        Delivery.Message.Builder msg = Delivery.Message.newBuilder();

        msg.setIdMessage(m.getIdMessage());
        msg.setType(m.getType());
        msg.setCounter(counterRemaining);                                   // => here the counter - 1
        msg.setTimestampCreation(m.getTimestampCreation());

        msg.setTopologyMessage(m.getTopologyMessage());
        msg.setFindMaster(m.getFindMaster());
        msg.setElection(m.getElection());
        msg.setRoundOfSignatures(m.getRoundOfSignatures());
        msg.setElected(m.getElected());
        msg.setBatteryPositionStatus(m.getBatteryPositionStatus());
        msg.setDeliveryPackage(m.getDeliveryPackage());
        msg.setDeliveryCompleted(m.getDeliveryCompleted());
        msg.setRechargeMessage(m.getRechargeMessage());
        msg.setAnswerRechargeMessage(m.getAnswerRechargeMessage());
        msg.setAreYouOk(m.getAreYouOk());
        msg.setImOk(m.getImOk());
        msg.setResetInternalStructure(m.getResetInternalStructure());
        msg.setExitMessage(m.getExitMessage());
        msg.setAnswerExitMessage(m.getAnswerExitMessage());
        msg.setOrderPending(m.getOrderPending());

        return msg;
    }

    private static void printSubLevelMessage(String message) {
        System.out.println("    " + ". " + message);
    }
}
