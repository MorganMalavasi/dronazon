package DRONE;

import io.grpc.stub.StreamObserver;

import java.util.logging.Logger;

class TopologyServiceImpl extends DeliveryServiceGrpc.DeliveryServiceImplBase {
    private final int id;
    private Queue q;
    private Logger logger = null;

    TopologyServiceImpl(int id, Queue q, Logger logger) {
        this.id = id;
        this.q = q;
        this.logger = logger;
    }

    @Override
    public StreamObserver<Delivery.Message> delivery(StreamObserver<Delivery.Message> responseObserver) {
        return new StreamObserver<Delivery.Message>() {
            @Override
            public void onNext(Delivery.Message message) {

                int counter = message.getCounter();
                if (counter >= 0){
                    // decrease the counter
                    Delivery.Message.Builder newMessage = MessageCreator.recreateMessage(message);

                    q.put(newMessage.build());
                } else {
                    // don't put the message in the queue
                    System.out.println("Counter stop");
                }
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {
                System.out.println("Client is disconnecting");
                // create message to recreate
            }
        };
    }
}
