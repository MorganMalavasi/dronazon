package DRONE;

import UTILS.Utils;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class GrpcServer extends Thread {
    private final int id;
    private final String ip;
    private final int portDrone;
    private Queue q;
    private Server server;
    private Logger logger = null;

    GrpcServer(int id, String ip, int portDrone, Queue q, Logger logger) {
        this.id = id;
        this.ip = ip;
        this.portDrone = portDrone;
        this.q = q;
        this.logger = logger;
    }

    @Override
    public void run() {
        System.out.println("Start server on port -> " + portDrone);

        server = ServerBuilder.forPort(portDrone)
                .addService(new TopologyServiceImpl(id, q, logger))
                .addService(new RechargeServiceImpl(id, q))
                .build();

        try {
            server.start();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            System.out.println("Server started successfully");
        }
    }

    public void stopServer() {
        server.shutdown();
        Utils.printClosingThread("SERVER");
        logger.log(Level.WARNING, "Closing server...");
    }
}