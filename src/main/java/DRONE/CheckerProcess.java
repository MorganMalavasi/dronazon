package DRONE;

import BEANS.ChannelsDirect;
import BEANS.ListToMaintain;
import BEANS.MapIdPort;

import java.util.List;

/**
 * loop without an end, every totSeconds check the status of the drones
 * if a drone is dead then he try to enter a new topology in the ring
 * so, all the drones updates
 **/
class CheckerProcess extends Thread {

    private int id;
    private List<ChannelsDirect> channels;
    private List<MapIdPort> listDroneMapped;

    private boolean firstEntry = true;
    private volatile boolean computing = true;
    private volatile boolean willOfContinuing = true;
    private Object lock = new Object();

    CheckerProcess(int id, List<ChannelsDirect> channels, List<MapIdPort> listDroneMapped){
        this.id = id;
        this.channels = channels;
        this.listDroneMapped = listDroneMapped;
    }

    @Override
    public void run() {
        while (willOfContinuing){
            try {
                if (!firstEntry){

                    // send a message to each drone to check if it is alive
                    Delivery.Message.Builder areYouOkMessageQuestion = MessageCreator.createAreYouOkMessageQuestion(id);
                    channels.forEach(element -> {
                        element.sendMessage(areYouOkMessageQuestion.build());
                    });
                }

                // wait ten seconds before to send another request
                synchronized (lock){
                    lock.wait(20000);       // 20 seconds of delay
                }

                // check if all the drones has confirmed their presence
                if (!firstEntry && computing){
                    // boolean b = ListToMaintain.checkCheck();
                    // evaluate(b);

                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }



            firstEntry = false;
        }
    }

    private void evaluate(boolean b) {
        if (b) System.out.println("all the drones are present"); else System.out.println("something changed");
    }


    public void stopCheckerLoop(){
        willOfContinuing = false;
        computing = false;
        synchronized (lock) {
            lock.notify();
        }
    }
}
