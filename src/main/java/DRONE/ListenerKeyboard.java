package DRONE;

import UTILS.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ListenerKeyboard extends Thread{

    private Drone_rpc drone_rpc;
    private int id;
    private Queue q;

    private volatile boolean continueReading = true;

    public ListenerKeyboard(Drone_rpc drone_rpc, int id, Queue q){
        this.drone_rpc = drone_rpc;
        this.id = id;
        this.q = q;
    }

    @Override
    public void run() {
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
        reading(buffReader);
        Utils.printClosingThread("LISTENING KEYBOARD");
    }

    private void reading(BufferedReader buffReader){
        while (continueReading) {
            String message = null;
            try {
                message = buffReader.readLine();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

            switch (message){
                case "quit":
                    continueReading = false;
                    drone_rpc.stopDroneClientServer();
                    break;
                case "recharge":
                    Delivery.Message.Builder rechargeMessage = MessageCreator.createRechargeMessageAsk(-1, System.currentTimeMillis());
                    q.putWithPriority(rechargeMessage.build());
                    break;
                case "stop":
                    System.exit(1);
                    break;

                default:
                    break;
            }
        }
    }
}
