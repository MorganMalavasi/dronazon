package DRONE;

import io.grpc.stub.StreamObserver;

class RechargeServiceImpl extends RechargeServiceGrpc.RechargeServiceImplBase{

    private int id;
    private Queue q;

    RechargeServiceImpl(int id, Queue q){
        this.id = id;
        this.q = q;
    }

    @Override
    public StreamObserver<Delivery.Message> recharge(StreamObserver<Delivery.Message> responseObserver) {
        return new StreamObserver<Delivery.Message>() {
            @Override
            public void onNext(Delivery.Message message) {
                int counter = message.getCounter();
                if (counter >= 0){
                    // decrease the counter
                    Delivery.Message.Builder newMessage = MessageCreator.recreateMessage(message);

                    q.putWithPriority(newMessage.build());
                } else {
                    // don't put the message in the queue
                    System.out.println("Counter stop");
                }
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {

            }
        };
    }
}
