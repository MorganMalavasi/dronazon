package DRONE;

import BEANS.ChannelsDirect;
import UTILS.Utils;

import java.util.List;

public class Timeout extends Thread {

    private boolean forceExit = true;
    private GrpcClient grpcClient;
    private int id;
    private List<ChannelsDirect> channelsDirects;
    private GrpcClient.EnterExit flag;

    public Timeout(GrpcClient grpcClient, int id, List<ChannelsDirect> channelsDirects, GrpcClient.EnterExit flag) {
        this.grpcClient = grpcClient;
        this.id = id;
        this.channelsDirects = channelsDirects;
        this.flag = flag;
    }

    @Override
    public void run() {

        // wait for 45 seconds
        wait25Seconds();

        if (forceExit) {

            if (flag == GrpcClient.EnterExit.ENTERING) {
                System.out.println("TIMEOUT ENTERING TRIGGERED");
                long time = System.currentTimeMillis();
                channelsDirects.stream().parallel().forEach(element -> {
                    if (element.getId() != id) {
                        Delivery.Message.Builder update = MessageCreator.createMessageTopology(element.getId(), time, grpcClient.getListDronesMapped(), false);
                        element.sendMessage(update.build());
                    }
                });
            }

            if (flag == GrpcClient.EnterExit.EXITING) {
                System.out.println("TIMEOUT EXITING TRIGGERED");
                Utils.removeDroneFromServer(id);
                System.exit(0);
            }
        }
    }

    private synchronized void wait25Seconds() {
        try {
            this.wait(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void notifyThread() {
        forceExit = false;
        this.notify();
    }

    public boolean isForceExit() {
        return forceExit;
    }

    public void setForceExit(boolean forceExit) {
        this.forceExit = forceExit;
    }
}
