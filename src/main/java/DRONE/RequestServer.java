package DRONE;

import BEANS.Drone;
import com.google.gson.Gson;
import com.sun.jersey.api.client.*;

public class RequestServer {
    public static ClientResponse postRequest(Client client, String url, Drone drone) {
        WebResource webResource = client.resource(url);
        String input = new Gson().toJson(drone);
        System.out.println(input);
        try {
            return webResource.type("application/json").post(ClientResponse.class, input);
        } catch (ClientHandlerException e) {
            System.out.println("Server offline");
            return null;
        }
    }

    public static ClientResponse deleteRequest(Client client, String url, Drone drone) {
        WebResource webResource = client.resource(url);
        String input = new Gson().toJson(drone);
        try {
            return webResource.type("application/json").delete(ClientResponse.class, input);
        } catch (ClientHandlerException e){
            System.out.println("Server offline");
            return null;
        }
    }

    public static ClientResponse deleteRequestId(Client client, String url) {
        WebResource webResource = client.resource(url);

        try {
            return webResource.delete(ClientResponse.class);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
