package DRONAZON;

import BEANS.Order;
import com.google.gson.Gson;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.nio.charset.StandardCharsets;
import java.util.Random;

class MqttPublisher {

    private static boolean stopper = false;

    public static void main(String[] args) {
        MqttClient client;
        String broker = "tcp://localhost:1883";
        String clientId = MqttClient.generateClientId();
        String topic = "dronazon/smartcity/orders";
        int qos = 2;

        try {
            client = new MqttClient(broker, clientId);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            // Connect the client
            System.out.println(clientId + " Connecting Broker " + broker);
            client.connect(connOpts);
            System.out.println(clientId + " Connected");

            // the program will generate a new incoming order each 5 seconds
            while(!stopper){
                MqttMessage message = new MqttMessage();
                String payload = generateNewPayload();
                message.setPayload(payload.getBytes(StandardCharsets.UTF_8));

                // Set the QoS on the Message
                message.setQos(qos);
                System.out.println(clientId + " Publishing message: " + payload + " ...");
                client.publish(topic, message);
                System.out.println(clientId + " Message published");
                Thread.sleep(5000);
            }

            if (client.isConnected())
                client.disconnect();
            System.out.println("Publisher " + clientId + " disconnected");


        } catch (MqttException me ) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        } catch (InterruptedException e) {
            System.err.println("Error in the sleep");
            e.printStackTrace();
        }
    }

    private static String generateNewPayload() {

        // creation of the variables
        Random rand = new Random();
        int id_message = rand.nextInt(100000);
        int pick_upX = rand.nextInt(10);
        int pick_upY = rand.nextInt(10);
        int deliveryX = rand.nextInt(10);
        int deliveryY = rand.nextInt(10);

        // json builder
        Gson gson = new Gson();
        String json = gson.toJson(new Order(id_message, pick_upX, pick_upY, deliveryX, deliveryY));
        return json;
    }
}

