package BEANS;

import java.util.ArrayList;
import java.util.List;

public class ListToMaintain {
    private static List<MapIdBoolean> thisList = null;

    public synchronized static void instance() {
        if (thisList == null)
            thisList = new ArrayList<>();
    }

    public synchronized static void setList(List<MapIdPort> list) {
        thisList.clear();
        for (int i = 0; i < list.size(); i++) {
            thisList.add(new MapIdBoolean(list.get(i).getId(), false));
        }
    }

    public synchronized static void updateList(List<MapIdPort> list) {
        // this method is used because if we use setList we clear completely the list and all the drones are set to false
        // this causes inconsistency because in that moment we could have some drone that they confirmed their presence
        // and others that they don't have yet confirmed

        List<MapIdBoolean> newList = new ArrayList<>();


        for (int i = 0; i < list.size(); i++) {
            boolean contains = false;
            MapIdBoolean obj = null;
            for (int j = 0; j < thisList.size(); j++) {
                if (list.get(i).getId() == thisList.get(j).getId()) {
                    contains = true;
                    obj = thisList.get(j);
                }
            }
            if (contains)
                newList.add(obj);
            else
                newList.add(new MapIdBoolean(list.get(i).getId(), true));
        }

        thisList.clear();
        thisList = newList;
    }

    public synchronized static List<MapIdBoolean> getList() {
        return thisList;
    }

    public synchronized static void setCheck(int id) {
        for (int i = 0; i < thisList.size(); i++)
            if (thisList.get(i).getId() == id)
                thisList.get(i).setCheck(true);
    }

    public synchronized static boolean checkCheck() {
        boolean see = true;

        for (int i = 0; i < thisList.size(); i++) {
            if (thisList.get(i).getCheck() == false) {
                see = false;
                break;
            }
        }

        return see;
    }
}
