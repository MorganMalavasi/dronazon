package BEANS;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Drone {
    private int id;
    private String ip;
    private int portDrone;
    private int posX;
    private int posY;
    private long millis;

    public Drone(){}

    public Drone(int id, String ip, int portdrones) {
        this.id = id;
        this.ip = ip;
        this.portDrone = portdrones;
        millis = System.currentTimeMillis();
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPortDrone() {
        return portDrone;
    }

    public void setPortDrone(int portDrone) {
        this.portDrone = portDrone;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
