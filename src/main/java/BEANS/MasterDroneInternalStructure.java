package BEANS;

import SIMULATORI.Measurement;

import java.util.ArrayList;
import java.util.List;

public class MasterDroneInternalStructure {
    private int id;
    private int battery;
    private int posX;
    private int posY;
    private boolean status;     // true if free, false if is occupied
    private int totKM;
    private int nrDeliveries;
    private List<Measurement> measurementList;

    public MasterDroneInternalStructure(int id, int battery, int posX, int posY, boolean status, int totKM, int nrDeliveries){
        this.id = id;
        this.battery = battery;
        this.posX = posX;
        this.posY = posY;
        this.status = status;
        this.totKM = totKM;
        this.nrDeliveries = nrDeliveries;
        this.measurementList = new ArrayList<>();
    }


    public int getId() {
        return id;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getTotKM() {
        return totKM;
    }

    public void setTotKM(int totKM) {
        this.totKM = totKM;
    }

    public int getNrDeliveries() {
        return nrDeliveries;
    }

    public void setNrDeliveries(int nrDeliveries) {
        this.nrDeliveries = nrDeliveries;
    }

    public List<Measurement> getMeasurementList() {
        return measurementList;
    }

    public void setMeasurementList(List<Measurement> measurementList) {
        this.measurementList = measurementList;
    }
}
