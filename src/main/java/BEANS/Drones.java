package BEANS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD)
public class Drones {
    @XmlElement(name="drone")
    private List<Drone> droneslist;

    private static Drones instance;

    private Drones() {
        droneslist = new ArrayList<>();
    }

    // singleton
    public synchronized static Drones getInstance(){
        if (instance == null)
            instance = new Drones();
        return instance;
    }

    // get the list of drones
    public synchronized List<Drone> getDroneslist(){
        return new ArrayList<>(droneslist);
    }

    // set the list of drones
    public synchronized void setDroneslist(List<Drone> droneslist){
        this.droneslist = droneslist;
    }

    // add a new drone to the list
    public synchronized void add(Drone d){
        droneslist.add(d);
    }
    // delete a drone from the list
    public synchronized boolean delete(Drone drone) {
        boolean operation = droneslist.removeIf(element -> element.getId() == drone.getId());
        return operation;
    }

    public synchronized boolean deleteId(int id) {
        boolean operation = droneslist.removeIf(element -> element.getId() == id);
        return operation;
    }

    // check if a drone with same id already exists
    public synchronized boolean checkDrone(Drone d){
        boolean check = false;
        for (Drone drone : droneslist){
            if (drone.getId() == d.getId())
                check = true;
        }
        return check;
    }

}
