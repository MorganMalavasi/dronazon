package BEANS;

public class MapIdBoolean {
    private int id;
    private boolean check;

    public MapIdBoolean(int id, boolean check){
        this.id = id;
        this.check = check;
    }

    public int getId() {
        return id;
    }


    public boolean getCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
