package SIMULATORI;

import java.util.*;

public class BufferImpl implements Buffer {
    private final String ID;
    private String type = "PM10";
    private int counter = 1;
    Deque<Measurement> listData = new ArrayDeque<>();
    ArrayList<Measurement> queue = new ArrayList<>();

    // constructor
    public BufferImpl(String id){
        ID = id;
    }

    @Override
    public synchronized void addMeasurement(Measurement m) {
        queue.add(m);
        if (queue.size() == 8) {
            listData.add(sum());
        }
    }

    @Override
    public synchronized List<Measurement> readAllAndClean() {
        List<Measurement> finalList = new ArrayList<>();
        finalList.addAll(listData);
        listData.clear();
        return finalList;
    }

    public synchronized Measurement sum() {
        double sumAverage = 0;
        long timestampAverage = 0;
        int window = 8;
        for (int i = 0; i < window; i++) {
            sumAverage += queue.get(i).getValue();
            timestampAverage += queue.get(i).getTimestamp();
        }

        Measurement m = new Measurement(ID + counter++, type, sumAverage / window, timestampAverage / window);

        int counter = 4;
        while (!(counter == 0)) {
            queue.remove(0);
            counter--;
        }

        return m;
    }

}

