import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;


import java.io.IOException;

public class ServerAdministrator {

    // accept request from the drone for entering in the group
    // the server return list of ID, IP, number of port of each drone actually present

    private static final String HOST = "localhost";
    private static final int PORT = 6789;

    public static void main (String[] args) throws IOException {
        // start administrator server on port 6789
        HttpServer server = HttpServerFactory.create("http://" + HOST + ":" + PORT + "/");
        server.start();

        System.out.println("Server Running");
        System.out.println("Server started on: " + "http://" + HOST + ":" + PORT);

        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server...");
        server.stop(0);
        System.out.println("Server stopped");

    }
}
